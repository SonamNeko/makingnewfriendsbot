package io.sonam.bot

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.sun.corba.se.impl.activation.CommandHandler
import io.sonam.bot.api.BotData
import io.sonam.bot.api.Register
import io.sonam.bot.handler.ChannelProgramHandler
import io.sonam.bot.handler.CommandsHandler
import io.sonam.bot.handler.ReactionRoleHandler
import io.sonam.jda.extensions.buildJda
import io.sonam.jda.handler.AsyncListenerAdapter
import net.dv8tion.jda.core.entities.impl.RoleImpl
import net.dv8tion.jda.core.events.guild.GuildReadyEvent
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.scanners.TypeAnnotationsScanner
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.primaryConstructor

class Bot {

    companion object {
        val JACKSON = ObjectMapper().registerKotlinModule()
        val Reflections = Reflections("io.sonam.bot", TypeAnnotationsScanner(), SubTypesScanner())
        lateinit var instance: Bot
    }

    val jda = buildJda {
        setToken(System.getenv("DISCORD_TOKEN"))

        Reflections.getTypesAnnotatedWith(Register::class.java).map { it.kotlin }
            .filter { it.findAnnotation<Register>()!!.type == Register.Type.Handler }
            .forEach { addEventListener(it.primaryConstructor!!.call()) }
    }

    val data = BotData
        .load()
        .setupCommands()
        .setSaveInterval(2500)

    init {
        instance = this
    }



}
