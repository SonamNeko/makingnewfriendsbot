package io.sonam.bot.handler

import io.sonam.bot.Bot
import io.sonam.bot.api.Command
import io.sonam.bot.api.Register
import io.sonam.jda.extensions.buildEmbed
import io.sonam.jda.handler.AsyncListenerAdapter
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.awt.Color

@Register(Register.Type.Handler)
class CommandsHandler : AsyncListenerAdapter() {

    private val split_regex = Regex("\\s+")

    companion object {
        val commands = hashSetOf<Command>()
    }

    override suspend fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        //mnf if(event.guild.idLong != Bot.instance.data.dev_server || event.guild.idLong != Bot.instance.data.main_server) return
        var message = event.message.contentDisplay
        if(message.startsWith(Bot.instance.data.prefix, false)) {
            Mono.create<Unit> {
                message = message.drop(Bot.instance.data.prefix.length)
                var split = message.split(split_regex)
                val command = split[0]
                split = if(split.size == 1) listOf() else split.drop(1)
                Flux.fromIterable(commands).filter { it.labels.contains(command) }.subscribe { cmd ->
                    if(cmd.evaluatePermissions(event.member)) {
                        if(split.size < cmd.minArgs()) {
                            event.message.channel.buildEmbed {
                                setColor(Color.RED)
                                setDescription("<:disagree:507698694737362944> **Usage:** ${Bot.instance.data.prefix}$command ${cmd.usage()}")
                            }.submit()
                        } else {
                            cmd.handle(event.message, split)
                        }
                    }
                }
            }.subscribe()
        }
    }
}
