package io.sonam.bot.handler

import io.sonam.bot.Bot
import io.sonam.bot.api.BotData
import io.sonam.bot.api.Cache
import io.sonam.bot.api.Register
import io.sonam.bot.api.hasAnyRoles
import io.sonam.jda.handler.AsyncListenerAdapter
import kotlinx.coroutines.runBlocking
import net.dv8tion.jda.core.entities.impl.GuildImpl
import net.dv8tion.jda.core.entities.impl.RoleImpl
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent
import org.apache.commons.codec.digest.DigestUtils
import reactor.core.publisher.Mono
import java.lang.Exception
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.schedule

@Suppress("IMPLICIT_CAST_TO_ANY")
@Register(Register.Type.Handler)
class ReactionRoleHandler : AsyncListenerAdapter() {

    override suspend fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        if(event.user.isBot) return
        val query = if(event.reactionEmote.isEmote) event.reactionEmote.id else event.reactionEmote.name
        runBlocking {
            Bot.instance.data.reactionRoleMessages[DigestUtils.md2Hex("${event.channel.idLong}${event.messageIdLong}$query")]?.let {
                try {
                    if(it.restrictionRoles != null) {
                        if(it.restrictionRoles!!.isNotEmpty())  {
                            if(event.member.hasAnyRoles(it.restrictionRoles!!)) {
                                event.guild.controller.addSingleRoleToMember(event.member, event.guild.getRoleById(it.role_id)).submit()
                            } else {
                                event.reaction.removeReaction(event.user).submit()
                            }
                        } else {
                            val msgData = Bot.instance.data.rrMessageConfig.getOrPut(event.messageIdLong) { BotData.MessageConfig() }
                            if(msgData.singleRole && event.member.hasAnyRoles(msgData.assignedRoles)) {
                                event.reaction.removeReaction(event.user).submitAfter(750, TimeUnit.MILLISECONDS)
                            } else {
                                event.guild.controller.addSingleRoleToMember(event.member, event.guild.getRoleById(it.role_id)).submit()
                            }
                        }
                    } else {
                        val msgData = Bot.instance.data.rrMessageConfig.getOrPut(event.messageIdLong) { BotData.MessageConfig() }
                        if(msgData.singleRole && event.member.hasAnyRoles(msgData.assignedRoles)) {
                            Timer().schedule(750) {
                                event.reaction.removeReaction(event.user).submit()
                            }
                        } else {
                            event.guild.controller.addSingleRoleToMember(event.member, event.guild.getRoleById(it.role_id)).submit()
                        }
                    }
                } catch (ex: Exception) {}
            }
        }
    }

    override suspend fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) {
        if(event.user.isBot) return
        val query = if(event.reactionEmote.isEmote) event.reactionEmote.id else event.reactionEmote.name
        Mono.create<Unit> { _ ->
            Bot.instance.data.reactionRoleMessages[DigestUtils.md2Hex("${event.channel.idLong}${event.messageIdLong}$query")]?.let {
                try {
                    event.guild.controller.removeSingleRoleFromMember(event.member, event.guild.getRoleById(it.role_id)).submit()
                } catch (ex: Exception) {}
            }
        }.subscribe()
    }

    override suspend fun onGuildMessageDelete(event: GuildMessageDeleteEvent) {
        Bot.instance.data.reactionRoleMessages.entries.removeIf { it.value.message_id == event.messageIdLong }
    }
}
