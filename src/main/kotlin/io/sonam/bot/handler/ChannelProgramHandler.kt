package io.sonam.bot.handler

import io.sonam.bot.api.Cache
import io.sonam.bot.api.ChannelProgram
import io.sonam.bot.api.Register
import io.sonam.jda.handler.AsyncListenerAdapter
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent
import net.dv8tion.jda.core.events.role.RoleDeleteEvent
import reactor.core.publisher.Flux
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue

@Register(Register.Type.Handler)
class ChannelProgramHandler : AsyncListenerAdapter() {

    companion object {
        val Programs = ConcurrentHashMap<Long, ConcurrentLinkedQueue<ChannelProgram>>()
    }

    override suspend fun onRoleDelete(event: RoleDeleteEvent) { Cache.signalRemoveRole(event.role.idLong) }

    override suspend fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        Programs[event.channel.idLong]?.let { queue ->
            Flux.fromIterable(queue)
                .doOnNext{ it.handle(event) }.subscribe()
        }
    }

    override suspend fun onGuildMessageReactionAdd(event: GuildMessageReactionAddEvent) {
        Programs[event.channel.idLong]?.let { queue ->
            Flux.fromIterable(queue)
                .doOnNext{ it.handle(event) }.subscribe()
        }
    }

    override suspend fun onGuildMessageReactionRemove(event: GuildMessageReactionRemoveEvent) {
        Programs[event.channel.idLong]?.let { queue ->
            Flux.fromIterable(queue)
                .doOnNext{ it.handle(event) }.subscribe()
        }
    }
}
