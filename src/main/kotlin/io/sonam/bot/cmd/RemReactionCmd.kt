package io.sonam.bot.cmd

import io.sonam.bot.Bot
import io.sonam.bot.api.Command
import io.sonam.bot.api.OK
import io.sonam.bot.api.Register
import io.sonam.jda.extensions.buildEmbed
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import java.time.LocalDateTime

@Register(Register.Type.Command)
class RemReactionCmd : Command("remrole", "rem") {

    override fun handle(message: Message, args: List<String>) {
        val id = args[0]
        if(Bot.instance.data.reactionRoleMessages.containsKey(id)) {
            Bot.instance.data.reactionRoleMessages.remove(id)!!.also {
                Bot.instance.data.rrMessageConfig[it.message_id]?.assignedRoles?.remove(it.role_id)
                message.channel.buildEmbed {
                    setColor(OK)
                    setTitle("Reaction Role Removed")
                    addField("ID", id, false)
                    addField("Message", it!!.message_id.toString(), true)
                    addField("Role", "<@&${it.role_id}>", true)
                    addField("Emote", it.emote, true)
                    setTimestamp(LocalDateTime.now())
                }.submit()
            }
        }
    }

    override fun minArgs(): Int = 1

    override fun usage(): String = "<reaction id>"

    override fun evaluatePermissions(member: Member): Boolean = member.hasPermission(Permission.ADMINISTRATOR)
}
