package io.sonam.bot.cmd

import io.sonam.bot.api.Command
import io.sonam.bot.api.Register
import io.sonam.bot.cmd.programs.EditMessageProgram
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import java.lang.Exception

@Register(Register.Type.Command)
class EditReactionMessage : Command("edit") {

    override fun handle(message: Message, args: List<String>) {
        val channel: TextChannel? = if(message.mentionedChannels.isEmpty()) {
            try {
                message.guild.getTextChannelById(args[0])
            } catch (ex: Exception) {
                message.channel.sendMessage("<:disagree:507698694737362944> Could not find channel by id ${args[0]}").submit()
                null
            }
        } else {
            message.mentionedChannels[0]
        }

        val msg: Message? = try {
            channel!!.getMessageById(args[1]).submit().get()
        } catch (ex: Exception) {
            message.channel.sendMessage("<:disagree:507698694737362944> Could not find message by id ${args[1]}").submit()
            null
        }

        if(channel != null && msg != null) {
            EditMessageProgram(message.member, message.channel as TextChannel, msg, channel).start()
        }
    }

    override fun minArgs(): Int = 2

    override fun usage(): String = "<channel> <message id>"

    override fun evaluatePermissions(member: Member): Boolean = member.hasPermission(Permission.ADMINISTRATOR)
}
