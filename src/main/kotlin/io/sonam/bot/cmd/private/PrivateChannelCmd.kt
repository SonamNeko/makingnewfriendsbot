package io.sonam.bot.cmd.private

import io.sonam.bot.api.Command
import io.sonam.jda.extensions.buildVoiceChannel
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.impl.RoleImpl

class PrivateChannelCmd : Command("private", "p") {

    private val CommandMatcher = Regex("(\\d+)?\\s*(.*)?").toPattern()

    override fun handle(message: Message, args: List<String>) {
        val matcher = CommandMatcher.matcher(args.joinToString(" "))
        if(matcher.find()) {
            val slots = matcher.group(1) ?: 0
            val name = matcher.group(2) ?: "Private ${message.member.user.name}"

            message.guild.getCategoryById(501906830927200257).buildVoiceChannel(name) {
                if(slots == 0) {
                    addPermissionOverride(message.guild.publicRole, 0, Permission.ALL_CHANNEL_PERMISSIONS)
                } else {

                }
            }

        }
    }

    override fun minArgs(): Int = 0

    override fun usage(): String = "[slots] [name]"

    override fun evaluatePermissions(member: Member): Boolean = true
}
