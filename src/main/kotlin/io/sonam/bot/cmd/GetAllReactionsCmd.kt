package io.sonam.bot.cmd

import io.sonam.bot.Bot
import io.sonam.bot.api.Command
import io.sonam.bot.api.Register
import io.sonam.jda.extensions.buildEmbed
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageEmbed
import java.awt.Color
import java.lang.Exception

@Register(Register.Type.Command)
class GetAllReactionsCmd : Command("getall") {

    override fun handle(message: Message, args: List<String>) {
        val channel: Channel? = if(!message.mentionedChannels.isEmpty()) {
            message.mentionedChannels[0]
        } else {
            try {
                message.guild.getTextChannelById(args[0])
            } catch (ex: Exception) {
                message.channel.sendMessage("<:disagree:507698694737362944> is not a valid channel id")
                null
            }
        }

        val msg: Long? = if(args.size < 2) {
            null
        } else {
            args[1].toLongOrNull()
        }

        if(channel != null) {
            var filtered = Bot.instance.data.reactionRoleMessages.filterValues { it.channel_id == channel.idLong }
            //val embed: MessageEmbed
            msg?.let { filtered = filtered.filterValues { msg_ -> msg_.message_id == msg } }

            //val filtered = Bot.instance.data.reactionRoleMessages

            if(filtered.isEmpty()) {
                message.channel.sendMessage("<:disagree:507698694737362944> No reaction roles found").submit()
                return
            }

            val embed = buildEmbed {
                setColor(Color.GREEN)
                if(msg!=null) {
                    setTitle("Message $msg")
                } else {
                    setTitle("For channel #${channel.name}")
                }
                val map = hashMapOf<Long, String>()
                filtered.forEach {
                    if(map.containsKey(it.value.message_id)) {
                        map[it.value.message_id] += "`${it.key}` **>** ${it.value.emote} <@&${it.value.role_id}>\n"
                    } else {
                        map[it.value.message_id] = "`${it.key}` **>** ${it.value.emote} <@&${it.value.role_id}>\n"
                    }
                }.also {
                    if(msg==null) {
                        for(entry in map.entries) {
                            addField("Message ${entry.key}", entry.value, false)
                        }
                    } else {
                        setDescription(map[msg]!!)
                    }
                }
            }
            message.channel.sendMessage(embed).submit()
        }
    }

    override fun minArgs(): Int = 1

    override fun usage(): String = "<channel> [message id]"

    override fun evaluatePermissions(member: Member): Boolean = member.hasPermission(Permission.ADMINISTRATOR)

}
