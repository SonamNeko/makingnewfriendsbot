package io.sonam.bot.cmd.permission

import io.sonam.bot.Bot
import io.sonam.bot.api.Command
import io.sonam.bot.api.PEACH
import io.sonam.bot.api.Register
import io.sonam.jda.extensions.buildEmbed
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.Role
import java.awt.Color

@Register(Register.Type.Command)
class PermissionAddCmd : Command("p", "perm", "permissions") {

    override fun handle(message: Message, args: List<String>) {
        if(args.isEmpty()) {
            message.channel.buildEmbed {
                setColor(PEACH)
                setTitle("Current Permission Settings for All")
                Bot.instance.data.permissionMap.forEach {
                    val desc = it.value.joinToString(" ") { id -> "<@&$id>" }
                    addField(it.key, if(it.value.isEmpty()) "`ADMINISTRATOR Flag Only`" else desc, false)
                }
            }.submit()
           return
        }
        if(args.isNotEmpty()) {

            if(!Bot.instance.data.permissionMap.containsKey(args[0].toLowerCase())) {
                message.channel.sendMessage("<:disagree:507698694737362944> Command ${args[0]} does not exist").submit()
                return
            }

            if(args.size == 1 || args.size == 2) {
                message.channel.buildEmbed {
                    setColor(PEACH)
                    setTitle("Current Permission Setting for ${args[0]}")
                    val desc = Bot.instance.data.permissionMap[args[0]]!!.joinToString(" ") { "<@&$it>" }
                    setDescription(if(Bot.instance.data.permissionMap[args[0]]!!.isEmpty()) "`ADMINISTRATOR Flag Only`" else desc)
                }.submit()
                return
            }

            if(args.size >= 3) {
                val role: Role? = try {
                    message.guild.getRoleById(args[1])
                } catch (ex: Exception) {
                    val name = args.drop(2).joinToString(" ")
                    val found = message.guild.getRolesByName(name, true)
                    if(!found.isEmpty()) {
                        found[0]
                    } else {
                        message.channel.sendMessage("<:disagree:507698694737362944> Could not find role by query $name")
                        null
                    }
                }

                if(role != null) {
                    when (args[1].toLowerCase()) {
                        "add" -> {
                            Bot.instance.data.permissionMap[args[0].toLowerCase()]!!.add(role.idLong)
                            message.channel.sendMessage("Added ${role.asMention} to ${args[0]}'s permissions").submit()
                        }
                        "rem" -> {
                            if(!Bot.instance.data.permissionMap[args[0].toLowerCase()]!!.remove(role.idLong)) {
                                message.channel.sendMessage("${role.asMention} is not part of ${args[0]}'s permissions").submit()
                            } else {
                                message.channel.sendMessage("Removed ${role.asMention} from ${args[0]}'s permissions").submit()
                            }
                        }
                    }
                }
            }
        }

    }

    override fun minArgs(): Int = 0

    override fun usage(): String = "<cmd name> <role>"
}
