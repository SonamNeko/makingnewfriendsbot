package io.sonam.bot.cmd

import io.sonam.bot.Bot
import io.sonam.bot.api.BotData
import io.sonam.bot.api.Command
import io.sonam.bot.api.OK
import io.sonam.bot.api.Register
import io.sonam.jda.extensions.buildEmbed
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.*
import org.apache.commons.codec.digest.DigestUtils
import java.awt.Color
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.time.LocalDateTime

@Register(Register.Type.Command)
class AddReactionCmd : Command("ar", "addrole") {

    /**
     * mnf ar <channel> <message id> <emote> <role name or id>
     */

    override fun handle(message: Message, args: List<String>) {
        val emm = args[2]
        val channel: TextChannel? = if(message.mentionedChannels.isEmpty()) {
            try {
                message.guild.getTextChannelById(args[0])
            } catch (ex: Exception) {
                message.channel.sendMessage("<:disagree:507698694737362944> Could not find channel by id ${args[0]}").submit()
                null
            }
        } else {
            message.mentionedChannels[0]
        }

        val msg: Message? = try {
            channel!!.getMessageById(args[1]).submit().get()
        } catch (ex: Exception) {
            message.channel.sendMessage("<:disagree:507698694737362944> Could not find message by id ${args[1]}").submit()
            null
        }

        val emote: Any = if(message.emotes.isEmpty()) {
            emm
        } else {
            message.emotes[0]
        }
        val role: Role? = try {
            message.guild.getRoleById(args[3])
        } catch (ex: Exception) {
            val msg = args.drop(3).joinToString(" ")
            val rolesFound = message.guild.getRolesByName(msg, true)
            if(rolesFound.isEmpty()) {
                message.channel.sendMessage("<:disagree:507698694737362944> Could not find a role by ID or name $msg").submit()
                null
            } else {
                rolesFound[0]
            }
        }

        if(channel != null && msg != null && role != null) {

            val queryType: String = if(emote is Emote) {
                emote.id
            } else {
                emote as String
            }

            val qString = DigestUtils.md2Hex("${channel.idLong}${msg.idLong}$queryType")

            try {
                if(emote is Emote)
                    msg.addReaction(emote).submit()
                if(emote is String)
                    msg.addReaction(emote).submit()
            } catch (ex: IllegalArgumentException) {
                message.channel.sendMessage("Please use an emote from your own server or a Discord default emote").submit()
            } finally {
                Bot.instance.data.reactionRoleMessages[qString] = BotData.RoleEmote(
                    channel.idLong,
                    msg.idLong,
                    if(emote is Emote) emote.asMention else emote as String,
                    role.idLong
                )

                val msgSettings = Bot.instance.data.rrMessageConfig.getOrPut(msg.idLong) { BotData.MessageConfig() }
                msgSettings.assignedRoles.add(role.idLong)

                message.channel.buildEmbed {
                    setTitle("Added new Reaction Emote")
                    setColor(OK)
                    addField("Reaction ID", qString, false)
                    addField("Emote", if(emote is Emote) {
                        emote.asMention
                    } else { emote as String }, true)
                    addField("Role", role.asMention, true)
                    setTimestamp(LocalDateTime.now())
                }.submit()
            }
        }

    }

    override fun minArgs(): Int = 4

    override fun usage(): String = "<channel> <message id> <emote> <role>"

    override fun evaluatePermissions(member: Member): Boolean = member.hasPermission(Permission.ADMINISTRATOR)

}
