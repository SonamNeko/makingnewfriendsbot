package io.sonam.bot.cmd.programs

import io.sonam.bot.Bot
import io.sonam.bot.api.BotData
import io.sonam.bot.api.ChannelProgram
import io.sonam.bot.api.OK
import io.sonam.bot.api.PEACH
import io.sonam.jda.extensions.buildEmbed
import io.sonam.jda.extensions.buildMessage
import net.dv8tion.jda.core.entities.*
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import org.apache.commons.codec.digest.DigestUtils
import java.awt.Color
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.time.LocalDateTime
import java.util.regex.Matcher

class EditMessageProgram(val member: Member, channel: TextChannel, val tMsg: Message, val tChn: TextChannel) : ChannelProgram(channel) {

    companion object {
        private val EndRegex = Regex("end\\s+(\\d+)").toPattern()
    }

    override fun start() {
        super.start()
        channel.buildEmbed {
            setTitle("Message Reaction Edit Mode")
            descriptionBuilder.appendln("<:dot:511709023150800917> To add a reaction emote, type a message matching this pattern: `<emote> <role name or id>`")
            descriptionBuilder.appendln()
            descriptionBuilder.appendln("<:dot:511709023150800917> To remove a reaction emote, type `rem <reaction id>`")
            descriptionBuilder.appendln("")
            descriptionBuilder.appendln("<:dot:511709023150800917> To list all reactions type `getall`")
            descriptionBuilder.appendln("")
            descriptionBuilder.appendln("<:dot:511709023150800917> To exit edit mode type `end <program id>`")
            setFooter("Program ID: ${this@EditMessageProgram.programId}", channel.jda.selfUser.avatarUrl)
            setColor(PEACH)
        }.submit()
    }

    override fun handle(event: GuildMessageReceivedEvent) {
        if(event.member !== member) return
        val msg = event.message.contentDisplay
        if(msg.startsWith("end")) {
            val splt = msg.split(" ")
            if(splt.size == 2) {
                splt[1].trim().toIntOrNull()?.let { id ->
                    this.finish()
                    this.channel.buildEmbed {
                        setColor(Color.CYAN)
                        setTitle("Message Reaction Edit Mode")
                        setDescription("Exited Edit Mode **ID #$id**")
                    }.submit()
                }
            }
            return
        }
        if(msg.startsWith("remall", true)) {
            val collector = Bot.instance.data.reactionRoleMessages.filter { it.value.message_id == this.tMsg.idLong }.map { it.key }.toSet()
            this.channel.sendMessage("Removing...").queue { fMsg ->
                collector.forEach {
                    Bot.instance.data.reactionRoleMessages.remove(it)
                }.also {
                    fMsg.editMessage("Removed all reaction roles from message ID `${this.tMsg.idLong}` in channel ${this.tChn.asMention}").submit()
                }
            }
            return
        }
        if(msg.startsWith("rem")) {
            val splt = msg.split(" ")
            if(splt.size == 2) {
                splt[1].trim().let { id ->
                    if(Bot.instance.data.reactionRoleMessages.containsKey(id)) {
                        Bot.instance.data.reactionRoleMessages[id]?.let {
                            Bot.instance.data.rrMessageConfig[it.message_id]?.assignedRoles?.remove(it.role_id)
                            channel.buildEmbed {
                                setColor(OK)
                                setTitle("Reaction Role Removed")
                                addField("Role", "<@&${it.role_id}>", true)
                                addField("Emote", it.emote, true)
                                setFooter("Program ID: ${this@EditMessageProgram.programId}", channel.jda.selfUser.avatarUrl)
                            }.submit()
                        }
                        Bot.instance.data.reactionRoleMessages.remove(id)
                        return
                    } else {
                        channel.sendMessage("Not a valid reaction ID")
                    }
                }
            }
            return
        }
        if(msg.equals("sr", true)) {
            val msgSettings = Bot.instance.data.rrMessageConfig.getOrPut(this.tMsg.idLong) { BotData.MessageConfig() }
            msgSettings.singleRole = !msgSettings.singleRole
            event.message.channel.buildEmbed {
                setDescription("Set `Single Assignable Role` setting to **${msgSettings.singleRole}**")
            }.submit()
            return
        }
        if(msg.startsWith("restrict", true) || msg.startsWith("res", true)) {
            val split = msg.split(Regex("\\s+")).drop(1)
            /**
             * restrict <id> <role name or id>
             */
            if(split.size >= 3) {
                when(split[0].toLowerCase()) {
                    "clear" -> {
                        val reId = split[1]

                        if(!Bot.instance.data.reactionRoleMessages.containsKey(reId)) {
                            event.message.channel.sendMessage("Could not find reaction role by id `$reId`").submit()
                            return
                        } else {
                            Bot.instance.data.reactionRoleMessages[reId]?.restrictionRoles?.clear()
                        }
                    }
                    "add" -> {
                        val reId = split[1]

                        if(!Bot.instance.data.reactionRoleMessages.containsKey(reId)) {
                            event.message.channel.sendMessage("Could not find reaction role by id `$reId`").submit()
                            return
                        }

                        val roleRaw = split.drop(2).joinToString(" ")
                        val role: Role? = try {
                            event.guild.getRoleById(roleRaw.trim())
                        } catch (ex: Exception) {
                            val rolesFound = event.guild.getRolesByName(roleRaw, true)
                            if(rolesFound.isEmpty()) {
                                null
                            } else {
                                rolesFound[0]
                            }
                        }
                        if(role != null) {
                            if (Bot.instance.data.reactionRoleMessages[reId]!!.restrictionRoles == null) {
                                Bot.instance.data.reactionRoleMessages[reId]!!.restrictionRoles = mutableListOf()
                            }
                            Bot.instance.data.reactionRoleMessages[reId]!!.restrictionRoles!!.add(role.idLong).also {
                                event.message.channel.sendMessage("Added role restriction ${role.asMention} to reaction role id `$reId`").submit()
                            }
                        } else {
                            event.message.channel.sendMessage("Could not find role by query `$roleRaw`").submit()
                        }
                    }
                    "rem" -> {
                        val reId = split[1]

                        if(!Bot.instance.data.reactionRoleMessages.containsKey(reId)) {
                            event.message.channel.sendMessage("Could not find reaction role by id `$reId`").submit()
                            return
                        }

                        val roleRaw = split.drop(2).joinToString(" ")
                        val role: Role? = try {
                            event.guild.getRoleById(roleRaw.trim())
                        } catch (ex: Exception) {
                            val rolesFound = event.guild.getRolesByName(roleRaw, true)
                            if(rolesFound.isEmpty()) {
                                null
                            } else {
                                rolesFound[0]
                            }
                        }
                        if(role != null) {
                            if (Bot.instance.data.reactionRoleMessages[reId]!!.restrictionRoles != null || Bot.instance.data.reactionRoleMessages[reId]!!.restrictionRoles!!.isEmpty()) {
                                Bot.instance.data.reactionRoleMessages[reId]!!.restrictionRoles!!.remove(role.idLong).also {
                                    event.message.channel.sendMessage("Removed role restriction ${role.asMention} from reaction role id `$reId`").submit()
                                }
                            } else {
                                event.message.channel.sendMessage("Reaction role `$reId` has no restrictions").submit()
                            }

                        } else {
                            event.message.channel.sendMessage("Could not find role by query `$roleRaw`").submit()
                        }
                    }
                }
            }
            return
        }
        if(msg.equals("getall", true)) {
            channel.buildEmbed {
                setColor(PEACH)
                setTitle("Reaction Roles for ${this@EditMessageProgram.tMsg.id}")
                Bot.instance.data.reactionRoleMessages.filter { it.value.channel_id == tChn.idLong && it.value.message_id == tMsg.idLong }
                    .forEach {
                        this.descriptionBuilder.appendln("`${it.key}` **>** ${it.value.emote} <@&${it.value.role_id}> ${
                            it.value.restrictionRoles.let {  list ->
                                if(list == null) {
                                    ""
                                } else {
                                    if(list.isEmpty()) {
                                        ""
                                    } else {
                                        "Restrictions: " + list.joinToString(" ") { rest -> "<@&$rest>" }
                                    }
                                }
                            }
                        } ")
                    }
            }.submit()
            return
        }
        if(msg.matches(Regex("(.*)\\s+(\\[.*])"))) {
            val split = msg.split(" ")
            if(split.size >= 2) {
                val emote: Any = if(event.message.emotes.isEmpty()) {
                    split[0]
                } else {
                    event.message.emotes[0]
                }
                val role: Role? = try {
                    event.message.guild.getRoleById(split[1])
                } catch (ex: Exception) {
                    val msg = split.drop(1).joinToString(" ").replace(Regex("\\[|\\]"), "")
                    val rolesFound = event.message.guild.getRolesByName(msg, true)
                    if(rolesFound.isEmpty()) {
                        event.message.channel.sendMessage("<:disagree:507698694737362944> Could not find a role by ID or name $msg").submit()
                        null
                    } else {
                        rolesFound[0]
                    }
                }

                if(role != null) {
                    val queryType: String = if(emote is Emote) {
                        emote.id
                    } else {
                        emote as String
                    }

                    val qString = DigestUtils.md2Hex("${tChn.idLong}${tMsg.idLong}$queryType")

                    try {
                        if(emote is Emote)
                            tMsg.addReaction(emote).submit()
                        if(emote is String)
                            tMsg.addReaction(emote).submit()
                    } catch (ex: IllegalArgumentException) {
                        event.message.channel.sendMessage("Please use an emote from your own server or a Discord default emote").submit()
                    } finally {
                        Bot.instance.data.reactionRoleMessages[qString] = BotData.RoleEmote(
                            tChn.idLong,
                            tMsg.idLong,
                            if(emote is Emote) emote.asMention else emote as String,
                            role.idLong
                        )

                        val msgSettings = Bot.instance.data.rrMessageConfig.getOrPut(this.tMsg.idLong) { BotData.MessageConfig() }
                        msgSettings.assignedRoles.add(role.idLong)

                        event.message.channel.buildEmbed {
                            setTitle("Added new Reaction Emote")
                            setColor(OK)
                            addField("Reaction ID", "`$qString`", false)
                            addField("Emote", if(emote is Emote) {
                                emote.asMention
                            } else { emote as String }, true)
                            addField("Role", role.asMention, true)
                            setFooter("Program ID: ${this@EditMessageProgram.programId}", channel.jda.selfUser.avatarUrl)
                        }.submit()
                    }
                }

            }
        }

    }

}
