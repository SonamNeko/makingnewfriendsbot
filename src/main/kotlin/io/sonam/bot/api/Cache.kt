package io.sonam.bot.api

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Role

object Cache {

    private val roleMap = hashMapOf<Long, Role>()

    fun signalRemoveRole(role: Long) {roleMap.remove(role)}

    fun getRoleById(guild: Guild, long: Long): Role? {
        roleMap[long]?.let {
            return it
        } ?: run {
            return try {
                guild.getRoleById(long).let {
                    roleMap[it.idLong] = it
                    return it
                }
            } catch (ex: Exception) {
                roleMap.remove(long)
                null
            }
        }
    }

}
