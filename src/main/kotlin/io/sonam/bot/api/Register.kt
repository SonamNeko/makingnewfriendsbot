package io.sonam.bot.api

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class Register(val type: Type) {
    enum class Type {
        Command, Handler
    }
}
