package io.sonam.bot.api

import com.fasterxml.jackson.module.kotlin.readValue
import io.sonam.bot.Bot
import io.sonam.bot.handler.CommandsHandler
import java.io.File
import kotlin.concurrent.fixedRateTimer
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.primaryConstructor

data class BotData
(
    val prefix: String = "mnf!",
    val main_server: Long = 422617057054687243,
    val dev_server: Long = 501158046152523786,
    val rrMessageConfig: MutableMap<Long, MessageConfig> = mutableMapOf(),
    val reactionRoleMessages: HashMap<String, RoleEmote> = hashMapOf(),
    val permissionMap: MutableMap<String, MutableList<Long>> = CommandsHandler.commands.map { it::class.simpleName!!.toLowerCase() }
        .associate { Pair<String, MutableList<Long>>(it, mutableListOf()) }.toMutableMap()
)
{

    data class RoleEmote (
        val channel_id: Long,
        val message_id: Long,
        val emote: String,
        val role_id: Long,
        var restrictionRoles: MutableList<Long>? = null
    )

    data class MessageConfig (
        val assignedRoles: MutableList<Long> = mutableListOf(),
        var singleRole: Boolean = false
    )

    companion object {
        val ConfigFile = File("data.json")
        val Commands = mutableMapOf<String, Command>()
        fun load(): BotData {
            if(ConfigFile.exists()) {
                return try {
                    Bot.JACKSON.readValue(ConfigFile)
                } catch (ex: Exception) {
                    println("There was an exception loading the configuration file.")
                    BotData()
                }
            } else {
                BotData().let {
                    ConfigFile.createNewFile()
                    ConfigFile.writeText(Bot.JACKSON.writeValueAsString(it))
                    return it
                }
            }
        }
    }

    fun setSaveInterval(period: Long): BotData {
        fixedRateTimer("save-timer", true, 1500, period) {
            save()
        }
        return this
    }

    fun setupCommands(): BotData {
        Bot.Reflections.getTypesAnnotatedWith(Register::class.java).map { it.kotlin }
            .forEach {
                it.findAnnotation<Register>()?.let {reg ->
                    if (reg.type == Register.Type.Command) {
                        CommandsHandler.commands.add(it.primaryConstructor!!.call() as Command)
                        println("Registered Command ${it.qualifiedName}")
                        permissionMap.putIfAbsent(it.simpleName!!.toLowerCase(), mutableListOf())
                    }
                }
            }
        return this
    }

    fun save() {
        try {
            if(!ConfigFile.exists())
                ConfigFile.createNewFile()
        } finally {
            ConfigFile.writeText(Bot.JACKSON.writeValueAsString(this))
        }
    }

}
