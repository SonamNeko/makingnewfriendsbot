package io.sonam.bot.api

import io.sonam.bot.handler.ChannelProgramHandler
import kotlinx.coroutines.runBlocking
import net.dv8tion.jda.core.entities.Channel
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent
import java.lang.Exception
import java.util.concurrent.ConcurrentLinkedQueue

open class ChannelProgram(val channel: TextChannel) {

    companion object {
        var CurrentId = 0
    }

    val programId = CurrentId++

    open fun start() {
        try {
            if(ChannelProgramHandler.Programs[channel.idLong] == null)
                ChannelProgramHandler.Programs[channel.idLong] = ConcurrentLinkedQueue()
        } finally {
            ChannelProgramHandler.Programs[channel.idLong]!!.add(this)
        }

    }

    open fun handle(add: GuildMessageReactionAddEvent) {}

    open fun handle(remove: GuildMessageReactionRemoveEvent) {}

    open fun handle(event: GuildMessageReceivedEvent) {}

    fun finish() {
        ChannelProgramHandler.Programs[this.channel.idLong]?.remove(this)
    }

    override fun equals(other: Any?): Boolean {
        if(other is ChannelProgram) {
            if(other.programId == this.programId)
                return true
        }
        return false
    }

    override fun toString(): String {
        return this.programId.toString()
    }
}
