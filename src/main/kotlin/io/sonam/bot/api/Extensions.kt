package io.sonam.bot.api

import net.dv8tion.jda.core.entities.Member
import java.awt.Color

fun Member.hasAnyRoles(roles: List<Long>): Boolean {
    for(i in roles) {
        if(this.roles.any { it.idLong == i }) {
            return true
        }
    }
    return false
}

val PEACH = Color(255, 187, 160)
val OK = Color(219, 255, 153)
