package io.sonam.bot.api

import io.sonam.bot.Bot
import io.sonam.bot.handler.ChannelProgramHandler
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Message

abstract class Command(vararg val labels: String) {

    companion object {
        private val EMPTY_SET = mutableListOf<Long>()
    }

    abstract fun handle(message: Message, args: List<String>)

    abstract fun minArgs(): Int

    abstract fun usage(): String

    open fun evaluatePermissions(member: Member): Boolean {
        return member.hasPermission(Permission.ADMINISTRATOR) || member.hasAnyRoles(Bot.instance.data.permissionMap.getOrDefault(this::class.simpleName!!, EMPTY_SET))
    }

}
