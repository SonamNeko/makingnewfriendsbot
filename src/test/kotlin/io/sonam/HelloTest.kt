package io.sonam

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.apache.commons.codec.digest.DigestUtils
import org.junit.Test
import reactor.core.publisher.Flux
import kotlin.test.assertEquals

class HelloTest {

    class DataTest

    @Test
    fun environment() {
        println(System.getenv("DISCORD_TOKEN"))
    }

    @Test
    fun fluxTest() {
        Flux.fromIterable(mutableListOf("1", "2", "3")).subscribe {
            println(it)
        }
    }

    @Test
    fun test() {
        val CommandMatcher = Regex("(\\d+)?\\s*(.*)?").toPattern()
        val matcher = CommandMatcher.matcher("asdasddas")
        if(matcher.find()) {
            println(matcher.group(0))
            println(matcher.group(1))
            println(matcher.group(2))
        }
    }

    @Test
    fun regexTest() {
        val EndRegex = Regex("end\\s+(\\d+)").toPattern()
        val matcher = EndRegex.matcher("end 3")
        println(matcher.find())
        println(matcher.group(1))
    }

    @Test
    fun splitTest() {
        println("asdas asdasd   asd".split(Regex("\\s+")))
    }

}
